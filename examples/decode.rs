use dtmf::decoder::Decoder;
use rodio::{Decoder as RDecoder, Source};
use std::fs::File;
use std::io::BufReader;

fn main() {
	// Load in our audio samples
	// This can also be done in real time from the sound card
	let file = BufReader::new(File::open("data/dtmf_test.wav").unwrap());
	let source = RDecoder::new(file).unwrap();
	let samples = source.convert_samples();
	let sample_rate = samples.sample_rate();
	let data: Vec<f32> = samples.collect();

	// set up our decoder
	let mut decoder = Decoder::new(sample_rate, |tone, state| {
		println!("{tone:?}: {state:?}");
	});

	// can process all samples at once, or in smaller batches
	decoder.process(&data);
}
