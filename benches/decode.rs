use criterion::{black_box, criterion_group, criterion_main, Criterion};
use dtmf::decoder::Decoder;
use rodio::{Decoder as RDecoder, Source};
use std::fs::File;
use std::io::BufReader;

pub fn benchmark_decoder(c: &mut Criterion) {
	// load samples
	let file = BufReader::new(File::open("data/dtmf_test.wav").unwrap());
	let source = RDecoder::new(file).unwrap();
	let samples = source.convert_samples();
	let sample_rate = samples.sample_rate();
	let data: Vec<f32> = samples.collect();

	c.bench_function("decoder small batches", |b| {
		let mut decoder = Decoder::new(sample_rate, |tone, state| {
			black_box(tone);
			black_box(state);
		});

		b.iter(|| {
			for s in data.chunks(1) {
				decoder.process(s);
			}
		})
	});

	c.bench_function("decoder large batches", |b| {
		let mut decoder = Decoder::new(sample_rate, |tone, state| {
			black_box(tone);
			black_box(state);
		});

		b.iter(|| {
			for s in data.chunks(40_000) {
				decoder.process(s);
			}
		})
	});
}

criterion_group!(benches, benchmark_decoder);
criterion_main!(benches);
