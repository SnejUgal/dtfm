#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum Tone {
	Zero,
	One,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	A,
	B,
	C,
	D,
	Asterisk,
	Pound,
}

impl Tone {
	pub fn as_char(&self) -> char {
		use Tone::*;

		match self {
			Zero => '0',
			One => '1',
			Two => '2',
			Three => '3',
			Four => '4',
			Five => '5',
			Six => '6',
			Seven => '7',
			Eight => '8',
			Nine => '9',
			A => 'A',
			B => 'B',
			C => 'C',
			D => 'D',
			Asterisk => '*',
			Pound => '#',
		}
	}

	pub fn from_char(c: char) -> Option<Self> {
		let tone = match c {
			'0' => Tone::Zero,
			'1' => Tone::One,
			'2' => Tone::Two,
			'3' => Tone::Three,
			'4' => Tone::Four,
			'5' => Tone::Five,
			'6' => Tone::Six,
			'7' => Tone::Seven,
			'8' => Tone::Eight,
			'9' => Tone::Nine,
			'A' | 'a' => Tone::A,
			'B' | 'b' => Tone::B,
			'C' | 'c' => Tone::C,
			'D' | 'd' => Tone::D,
			'*' => Tone::Asterisk,
			'#' => Tone::Pound,
			_ => return None,
		};

		Some(tone)
	}
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum State {
	On,
	Off,
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn encode_decode_works() {
		let s_in = "1234567890abcdABCD*#";
		let expected = "1234567890ABCDABCD*#";

		for (c, e) in s_in.chars().zip(expected.chars()) {
			let actual_char = Tone::from_char(c).unwrap().as_char();

			assert_eq!(actual_char, e);
		}
	}

	#[test]
	fn decode_cant_decode_invalid() {
		let invalid = "the quik rown fox jumpe over the lzy og THE QUIK ROWN FOX JUMPE OVER THE LZY OG !@$%^&()-_=+[{]}\\|";

		for c in invalid.chars() {
			assert_eq!(None, Tone::from_char(c));
		}
	}
}
