// allow std in tests
#![cfg_attr(not(test), no_std)]

pub mod decoder;
pub mod enums;

#[cfg(test)]
mod tests {
	use crate::decoder::Decoder;
	use crate::enums::{State, Tone};
	use rodio::{Decoder as RDecoder, Source};
	use std::fs::File;
	use std::io::BufReader;

	// 12398754932387ABCD*#
	#[test]
	fn decoder_works() {
		let expected = vec![
			(Tone::One, State::On),
			(Tone::One, State::Off),
			(Tone::Two, State::On),
			(Tone::Two, State::Off),
			(Tone::Three, State::On),
			(Tone::Three, State::Off),
			(Tone::Nine, State::On),
			(Tone::Nine, State::Off),
			(Tone::Eight, State::On),
			(Tone::Eight, State::Off),
			(Tone::Seven, State::On),
			(Tone::Seven, State::Off),
			(Tone::Five, State::On),
			(Tone::Five, State::Off),
			(Tone::Four, State::On),
			(Tone::Four, State::Off),
			(Tone::Nine, State::On),
			(Tone::Nine, State::Off),
			(Tone::Three, State::On),
			(Tone::Three, State::Off),
			(Tone::Two, State::On),
			(Tone::Two, State::Off),
			(Tone::Three, State::On),
			(Tone::Three, State::Off),
			(Tone::Eight, State::On),
			(Tone::Eight, State::Off),
			(Tone::Seven, State::On),
			(Tone::Seven, State::Off),
			(Tone::A, State::On),
			(Tone::A, State::Off),
			(Tone::B, State::On),
			(Tone::B, State::Off),
			(Tone::C, State::On),
			(Tone::C, State::Off),
			(Tone::D, State::On),
			(Tone::D, State::Off),
			(Tone::Asterisk, State::On),
			(Tone::Asterisk, State::Off),
			(Tone::Pound, State::On),
		];

		let file = BufReader::new(File::open("data/dtmf_test.wav").unwrap());
		let source = RDecoder::new(file).unwrap();
		let samples = source.convert_samples();
		let sample_rate = samples.sample_rate();
		let data: Vec<f32> = samples.collect();

		// first extreme - very few samples
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(1) {
				decoder.process(s);
			}

			assert_eq!(expected, actual);
		}

		// second extreme - many samples
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(40_000) {
				decoder.process(s);
			}

			assert_eq!(expected, actual);
		}
	}

	#[test]
	fn decoder_works_noisy() {
		let file = BufReader::new(File::open("data/noisy_dtmf.mp3").unwrap());
		let source = RDecoder::new(file).unwrap();
		let samples = source.convert_samples();
		let sample_rate = samples.sample_rate();
		let data: Vec<f32> = samples.collect();

		// first extreme - very few samples
		let c1;
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(1) {
				decoder.process(s);
			}

			c1 = actual.len();
			log_sanity_check(actual);
		}

		// second extreme - many samples
		let c2;
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(40_000) {
				decoder.process(s);
			}

			c2 = actual.len();
			log_sanity_check(actual);
		}

		assert_eq!(c1, c2);
		assert_eq!(c1, 8047); // true value is 8063
	}

	#[test]
	fn no_false_positives() {
		// Sound taken from https://www.youtube.com/watch?v=YzG4RZNLuUk

		let file = BufReader::new(File::open("data/radio_chatter.mp3").unwrap());
		let source = RDecoder::new(file).unwrap();
		let samples = source.convert_samples();
		let sample_rate = samples.sample_rate();
		let data: Vec<f32> = samples.collect();

		// first extreme - very few samples
		let c1;
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(1) {
				decoder.process(s);
			}

			c1 = actual.len();
			log_sanity_check(actual);
		}

		// second extreme - many samples
		let c2;
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(40_000) {
				decoder.process(s);
			}

			c2 = actual.len();
			log_sanity_check(actual);
		}

		assert_eq!(c1, c2);
		assert_eq!(c1, 34); // ideally we want 0
	}

	// See issue #2
	#[test]
	fn dont_on_when_on_dont_off_when_off() {
		let expected = vec![(Tone::One, State::On), (Tone::One, State::Off)];

		let file = BufReader::new(File::open("data/dtmf_edge.wav").unwrap());
		let source = RDecoder::new(file).unwrap();
		let samples = source.convert_samples();
		let sample_rate = samples.sample_rate();
		let data: Vec<f32> = samples.collect();

		// first extreme - very few samples
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(1) {
				decoder.process(s);
			}

			assert_eq!(expected, actual);
		}

		// second extreme - many samples
		{
			let mut actual = vec![];
			let mut decoder = Decoder::new(sample_rate, |tone, state| {
				actual.push((tone, state));
			});

			for s in data.chunks(40_000) {
				decoder.process(s);
			}

			assert_eq!(expected, actual);
		}
	}

	fn log_sanity_check(mut log: Vec<(Tone, State)>) {
		// pad w/ off if last element is on
		if log.last().unwrap().1 == State::On {
			log.push((log.last().unwrap().0, State::Off));
		}

		// every on needs an off
		for c in log.chunks(2) {
			assert_eq!(c[0].0, c[1].0);
			assert_eq!(State::On, c[0].1);
			assert_eq!(State::Off, c[1].1);
		}
	}
}
