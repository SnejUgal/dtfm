# dtmf

A no-std DTMF decoder for Rust. Can be used on a microcontroller or on a normal PC.

## Usage

```rust
// Load in our audio samples
// This can also be done in real time from the sound card
let mut sample_file = File::open("data/dtmf_test.wav").unwrap();
let (header, data) = wav::read(&mut sample_file).unwrap();
let data = data.try_into_sixteen().unwrap();

// set up our decoder
let mut decoder = Decoder::new(header.sampling_rate, |tone, state| {
	println!("{tone:?}: {state:?}");
});

// can process all samples at once, or in smaller batches
decoder.process(&data);
```
